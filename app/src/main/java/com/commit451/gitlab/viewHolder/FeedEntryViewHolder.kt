package com.commit451.gitlab.viewHolder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import coil.api.load
import coil.transform.CircleCropTransformation
import com.commit451.gitlab.R
import com.commit451.gitlab.extension.formatAsHtml
import com.commit451.gitlab.model.rss.Entry
import com.commit451.gitlab.util.DateUtil

/**
 * Represents the view of an item in the RSS feed
 */
class FeedEntryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {

        fun inflate(parent: ViewGroup): FeedEntryViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_entry, parent, false)
            return FeedEntryViewHolder(view)
        }
    }

    @BindView(R.id.image)
    lateinit var image: ImageView
    @BindView(R.id.title)
    lateinit var textTitle: TextView
    @BindView(R.id.description)
    lateinit var textSummary: TextView
    @BindView(R.id.updated)
    lateinit var textUpdated: TextView

    init {
        ButterKnife.bind(this, view)
    }

    fun bind(entry: Entry) {
        image.load(entry.thumbnail.url) {
            transformations(CircleCropTransformation())
        }

        textTitle.text = entry.title.formatAsHtml()
        textSummary.text = entry.summary.formatAsHtml()
        val updatedRelative = DateUtil.getRelativeTimeSpanString(itemView.context, entry.updated)

        textUpdated.text = updatedRelative
    }
}
